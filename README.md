# freerunner2

bit trip presents runner 2 future legend of rhythm alien...

...but let's try to make it free ([as in freedom](https://www.debian.org/intro/free))

## the backstory
```
drm bad
runner2 good
installed the humble bundle version on my debian sid machine using [lutris](https://lutris.net/)
// humble bundle version info:
// archive name - runner2_amd64_1388171186.tar.gz
// archive md5 - 2f7ccdb675a63a5fc152514682e97480
got it running (this is kind of sounding like greentext rn)
all my controllers didn't work
got a 360 controller
it worked!
started playing the first level
constant pausing
tried playing with just keyboard
problem still there
looked at debug output
found "gnome-screensaver-command: not found"
installed gnome-screensaver
tried again, still happening
"** (gnome-screensaver-command:2670): WARNING **: 14:03:57.702: Unknown option -p"
grep to find gnome-screensaver in game folder
found it in the binary
tried to find stuff in a hex editor
decided that wasn't good enough
installed [ghidra](https://ghidra-sre.org/)
realized this is now turning into a decomp project a la [mario 64](https://github.com/n64decomp/sm64)
got inspired
made a repo
```

## the general idea
decompile runner2 so it's source code that can be played with

## the ```//todo``` list
### phase 1
- [ ] figure out how to patch runner2 using ghidra
- [ ] create patch to fix pausing issue
  - [ ] bonus: integrate patch into lutris install

### phase 1.5
- [ ] play runner2
- [ ] bonus: play with [libTAS](https://github.com/clementgallet/libTAS) and maybe try TASing it?

### phase 2
- [ ] do a full decompile of runner2 using ghidra
- [ ] figure out how to build it from source
- [ ] make scripts to build runner2 from source
  - [ ] bonus: make scripts fancy so we can download from humble and extract the assets etc
  
### phase 3
- [ ] figure out what it takes to clean up the code a la [OpenRCT2](https://openrct2.io/)
- [ ] get stuff into a decently editable state
- [ ] try adding a feature (support for more controllers maybe?)

# the 🐘 in the room/possible shortcut/open letter
*dear [Choice Provisions](https://totallychoice.com/),*

*i am a huge fan of runner2*

*i would love to be able to fix the bug i found in it*

*it would be a lot easier to do so if i had access to the source code*

*if you would be willing to share that with everyone,* 

*it would make me really happy*

*thank you for your time!*

*- brian*